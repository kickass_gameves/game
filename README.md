# Project goal

To create a rouge like, multiplayer game engine with some default implementation of the game. Project is made for pure fun and learning. 

We want to achieve high modularity of a game in many areas, such as
1. Map generation
1. Fight system
1. Monster stats and behaviour
1. Items stats and behaviour


# Technical layout

Whole project will be written in Java 8, no special frameworks are now being planned to be used (other then for testing). 
High modularity will be achieved by using Nashorn, modularized properties will be loaded from .js files during startup of the system.

Standard dev tools like maven will follow us everywhere

We do TDD, however it's a learning excercise so we will spike and experiment a lot.


If you hope to achieve some success with us, sorry, won't happen ;) 

*And we do not accept pull requests!* We're not arrogant, just we treat it as excercise for ourselves, to hangout afterhours and code for fun in PP mode over internet. If you want to contribute, send them, but expect we're going to use them in our own way :)
