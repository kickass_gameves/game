package eu.ppdevs.crawler.engine.board.grid;

import eu.ppdevs.crawler.engine.board.Coordinate;
import eu.ppdevs.crawler.engine.board.LocationId;

/**
 * User: Michal
 * Date: 2016-03-19
 */
class GridBoardCoordinate implements Coordinate, LocationId {
	private final GridBoardRow row;
	private final GridBoardColumn column;

	public GridBoardCoordinate(final GridBoardRow row, final GridBoardColumn column) throws IllegalArgumentException {
		if (row == null) throw new IllegalArgumentException("Invalid row : null");
		if (column == null) throw new IllegalArgumentException("Invalid column : null");
		this.row = row;
		this.column = column;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		final GridBoardCoordinate that = (GridBoardCoordinate) o;
		return row.equals(that.row) && column.equals(that.column);

	}

	@Override
	public int hashCode() {
		int result = row.hashCode();
		result = 31 * result + column.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "GridBoardCoordinate(" + row + ", " + column + ")";
	}
}
