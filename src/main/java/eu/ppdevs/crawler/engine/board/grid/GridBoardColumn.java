package eu.ppdevs.crawler.engine.board.grid;

/**
 * User: Michal
 * Date: 2016-03-19
 */
class GridBoardColumn {
	private final Integer coordinate;

	//Should this be a factory method?
	public GridBoardColumn(final Integer coordinate) {
		if (coordinate == null || coordinate < 0) throw new IllegalArgumentException("Column may not be negative");
		this.coordinate = coordinate;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		final GridBoardColumn that = (GridBoardColumn) o;

		return coordinate.equals(that.coordinate);

	}

	@Override
	public int hashCode() {
		return coordinate.hashCode();
	}

	@Override
	public String toString() {
		return "GridBoardColumn(" + coordinate + ")";
	}
}
