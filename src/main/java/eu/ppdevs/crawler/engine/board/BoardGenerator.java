package eu.ppdevs.crawler.engine.board;

/**
 * User: Michal
 * Date: 2016-03-15
 */
public interface BoardGenerator<T extends BoardProperties> {
	Board generateBoard(T boardProperties);
}
