package eu.ppdevs.crawler.engine.board;

/**
 * Container for everything that is located on given location of a board.
 *
 * It's unaware of it's own coordinate in space, neighbours or rest of the map
 *
 * User: Michal
 * Date: 2016-03-13
 */
public class Location {
	private final LocationId id;

	public Location(final LocationId id) {
		this.id = id;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		final Location location = (Location) o;

		return id.equals(location.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}


	@Override
	public String toString() {
		return "Location("+id+")";
	}
}
