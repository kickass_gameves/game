package eu.ppdevs.crawler.engine.board.grid;

import eu.ppdevs.crawler.engine.board.*;

import java.util.*;

/**
 * User: Michal
 * Date: 2016-03-15
 */
public class DefaultBoardGenerator implements BoardGenerator<GridBoardProperties> {

	@Override
	public Board generateBoard(final GridBoardProperties boardProperties) {
		final Integer rows = boardProperties.rows;
		final Integer columns = boardProperties.columns;
		Map<Coordinate, Location> locationList = new HashMap<>(rows*columns);
		for(int row = 0; row < rows; row++){
			for(int column = 0; column < columns; column++){
				final GridBoardCoordinate locationId = new GridBoardCoordinate(
						new GridBoardRow(row),
						new GridBoardColumn(column)
				);
				locationList.put(locationId, new Location(locationId));
			}
		}

		return new GridBoard(locationList);
	}
}
