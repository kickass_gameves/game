package eu.ppdevs.crawler.engine.board;

import java.util.Optional;
import java.util.Set;

/**
 * User: Michal
 * Date: 2016-03-13
 */
public interface Board {
	Optional<Location> get(Coordinate coordinate);
	Set<Location> getAdjacentFields(Coordinate coordinate);
}
