package eu.ppdevs.crawler.engine.board.grid;

import eu.ppdevs.crawler.engine.board.*;

import java.util.*;

import static java.util.Optional.ofNullable;

/**
 * User: Michal
 * Date: 2016-03-15
 */
class GridBoard implements Board {

	final Map<Coordinate, Location> locationMap;

	GridBoard(final Map<Coordinate, Location> locations) {
		this.locationMap = locations;
	}

	@Override
	public Optional<Location> get(final Coordinate coordinate) {
		return ofNullable(locationMap.get(coordinate));
	}

	@Override//TODO implement me! :)
	public Set<Location> getAdjacentFields(final Coordinate coordinate) {
		return null;
	}
}
