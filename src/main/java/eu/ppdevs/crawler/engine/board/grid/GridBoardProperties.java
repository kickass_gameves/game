package eu.ppdevs.crawler.engine.board.grid;

import eu.ppdevs.crawler.engine.board.BoardProperties;

/**
 * User: Michal
 * Date: 2016-03-15
 */
class GridBoardProperties implements BoardProperties {
	public final Integer rows;
	public final Integer columns;

	//private constructor to ensure builder usage
	private GridBoardProperties(Integer rows, Integer columns){
		this.rows = rows;
		this.columns = columns;
	}

	public static BoardPropertiesBuilder builder() {
		return new BoardPropertiesBuilder();
	}

	static class BoardPropertiesBuilder {
		int rows = 0;
		int columns = 0;

		public GridBoardProperties build() {
			return new GridBoardProperties(rows, columns);
		}

		public BoardPropertiesBuilder setRows(final Integer rows) {
			this.rows = rows;
			return this;
		}

		public BoardPropertiesBuilder setColumns(final Integer columns) {
			this.columns = columns;
			return this;
		}
	}
}
