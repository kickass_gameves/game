package eu.ppdevs.crawler.engine.board.grid;

import eu.ppdevs.crawler.engine.board.*;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * User: Michal
 * Date: 2016-03-19
 */
public class GridBoardTest {
	final int defaultDimension = 10;
	DefaultBoardGenerator generator;
	GridBoardProperties props;
	Board board;

	@Before
	public void setUp() throws Exception {
		props = GridBoardProperties.builder().setRows(defaultDimension).setColumns(defaultDimension).build();
		generator = new DefaultBoardGenerator();
		board = generator.generateBoard(props);
	}

	@Test
	public void boardReturns_Location_BasedOnCoordinate_IfItsOnBoard() throws Exception {
		GridBoardRow desiredRow = new GridBoardRow(1);
		GridBoardColumn desiredColumn = new GridBoardColumn(1);
		Location desiredLocation = new Location(new GridBoardCoordinate(desiredRow, desiredColumn));

		//then
		assertThat(board.get(new GridBoardCoordinate(desiredRow, desiredColumn)).get()).isEqualTo(desiredLocation);
	}

	@Test
	public void boardReturnsEmptyOptional_Location_BasedOnCoordinate_IfNotOnBoard() throws Exception {
		assertThat(board.get(new GridBoardCoordinate(new GridBoardRow(10), new GridBoardColumn(1))).isPresent()).isFalse();
	}

	@Test
	public void verifyBoardGenerationRules_extremeEndsTest() throws Exception {
		assertThat(board.get(new GridBoardCoordinate(new GridBoardRow(0), new GridBoardColumn(0)))
				.isPresent()).describedAs("(0,0) is the corner").isTrue();

		assertThat(board.get(new GridBoardCoordinate(new GridBoardRow(defaultDimension-1), new GridBoardColumn(0)))
				.isPresent()).describedAs("(max generated size-1, 0) is the corner").isTrue();

		assertThat(board.get(new GridBoardCoordinate(new GridBoardRow(0), new GridBoardColumn(defaultDimension-1)))
				.isPresent()).describedAs("(0, max generated size-1) is the corner").isTrue();

		assertThat(board.get(new GridBoardCoordinate(new GridBoardRow(0), new GridBoardColumn(defaultDimension-1)))
				.isPresent()).describedAs("(max generated size-1, max generated size-1) is the corner").isTrue();

		assertThat(board.get(new GridBoardCoordinate(new GridBoardRow(defaultDimension), new GridBoardColumn(0)))
				.isPresent()).describedAs("max generated value is out of the map").isFalse();

		assertThat(board.get(new GridBoardCoordinate(new GridBoardRow(0), new GridBoardColumn(defaultDimension)))
				.isPresent()).describedAs("max generated value is out of the map").isFalse();
	}
}