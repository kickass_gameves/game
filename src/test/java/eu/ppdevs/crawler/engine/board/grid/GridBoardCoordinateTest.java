package eu.ppdevs.crawler.engine.board.grid;

import eu.ppdevs.crawler.engine.board.Location;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

/**
 * User: Michal
 * Date: 2016-03-19
 */
public class GridBoardCoordinateTest {
	GridBoardRow row1;
	GridBoardRow row1a;
	GridBoardRow row2;
	GridBoardColumn column1;
	GridBoardColumn column1a;
	GridBoardColumn column2;
	GridBoardCoordinate coordinate1;
	GridBoardCoordinate coordinate1a;
	GridBoardCoordinate coordinate2;

	@Before
	public void setUp() throws Exception {
		row1 = new GridBoardRow(1);
		row1a = new GridBoardRow(1);
		row2 = new GridBoardRow(2);

		column1 = new GridBoardColumn(1);
		column1a = new GridBoardColumn(1);
		column2 = new GridBoardColumn(2);

		coordinate1 = new GridBoardCoordinate(row1, column1);
		coordinate1a = new GridBoardCoordinate(row1a, column1a);
		coordinate2 = new GridBoardCoordinate(row2, column2);
	}

	@Test
	public void gridCoordinatesAreDifferent_WhenColumnsAreDifferent() throws Exception {
		GridBoardCoordinate coordinate1 = new GridBoardCoordinate(row1, column1);
		GridBoardCoordinate coordinate2 = new GridBoardCoordinate(row1, column2);

		assertThat(coordinate1).isNotEqualTo(coordinate2);
	}

	@Test
	public void gridCoordinatesAreDifferent_WhenRowsAreDifferent() throws Exception {
		GridBoardCoordinate coordinate1 = new GridBoardCoordinate(row1, column1);
		GridBoardCoordinate coordinate2 = new GridBoardCoordinate(row2, column1);

		assertThat(coordinate1).isNotEqualTo(coordinate2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void gridCoordinateCantBeInstantiated_WithNull_Column() throws Exception {
		new GridBoardCoordinate(row1, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void gridCoordinateCantBeInstantiated_WithNull_Row() throws Exception {
		new GridBoardCoordinate(null, column1);
	}

	@Test
	public void gridCoordinateIs_AValueObject() throws Exception {
		assertThat(coordinate1).isEqualTo(coordinate1a).isNotEqualTo(coordinate2);
		assertThat(coordinate1.hashCode()).isEqualTo(coordinate1a.hashCode()).isNotEqualTo(coordinate2.hashCode());
		assertThat(coordinate1.toString()).isEqualTo(coordinate1a.toString()).isNotEqualTo(coordinate2.toString());
	}

	@Test
	public void gridCoordinate_IsValidLocationId() throws Exception {
		Location location1 = new Location(coordinate1);
		Location location1a = new Location(coordinate1a);
		Location location2 = new Location(coordinate2);

		assertThat(location1).isEqualTo(location1a).isNotEqualTo(location2);
		assertThat(location1.hashCode()).isEqualTo(location1a.hashCode()).isNotEqualTo(location2.hashCode());
		assertThat(location1.toString()).isEqualTo(location1a.toString()).isNotEqualTo(location2.toString());
	}
}