package eu.ppdevs.crawler.engine.board;

import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * User: Michal
 * Date: 2016-03-19
 */
public class BoardEntitiesTest {

	@Test
	public void locationIs_AValueObject() throws Exception {
		Location location1 = new Location(new StubLocationId(1));
		Location location1a = new Location(new StubLocationId(1));
		Location location2 = new Location(new StubLocationId(2));

		assertThat(location1).isEqualTo(location1a).isNotEqualTo(location2);
		assertThat(location1.hashCode()).isEqualTo(location1a.hashCode()).isNotEqualTo(location2.hashCode());
		assertThat(location1.toString()).isEqualTo(location1a.toString()).isNotEqualTo(location2.toString());
	}

	@Test@Ignore//TODO revisit when location will have something on it
	public void comparingLocationsWithSameId_ButDifferentObjectsOnIt_ThrowsException() throws Exception {


	}

	class StubLocationId implements LocationId {
		int id;

		public StubLocationId(final int id) {
			this.id = id;
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			final StubLocationId that = (StubLocationId) o;

			return id == that.id;

		}

		@Override
		public int hashCode() {
			return id;
		}

		@Override
		public String toString() {
			return "StubLocationId{" +
					"id=" + id +
					'}';
		}
	}
}