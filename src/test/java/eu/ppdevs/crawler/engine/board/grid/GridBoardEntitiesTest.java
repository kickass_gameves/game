package eu.ppdevs.crawler.engine.board.grid;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

/**
 * User: Michal
 * Date: 2016-03-19
 */
public class GridBoardEntitiesTest {

	@Test
	public void rowIs_AValueObject() throws Exception {
		GridBoardRow row1 = new GridBoardRow(1);
		GridBoardRow row1a = new GridBoardRow(1);
		GridBoardRow row2 = new GridBoardRow(2);

		assertThat(row1).isEqualTo(row1a).isNotEqualTo(row2);
		assertThat(row1.hashCode()).isEqualTo(row1a.hashCode()).isNotEqualTo(row2.hashCode());
		assertThat(row1.toString()).isEqualTo(row1a.toString()).isNotEqualTo(row2.toString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void rowCantBeInitializedWithNegativeNumber() throws Exception {
		new GridBoardRow(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void rowCantBeInitializedWithNull() throws Exception {
		new GridBoardRow(null);
	}

	@Test
	public void columnIs_AValueObject() throws Exception {
		GridBoardColumn column1 = new GridBoardColumn(1);
		GridBoardColumn column1a = new GridBoardColumn(1);
		GridBoardColumn column2 = new GridBoardColumn(2);

		assertThat(column1).isEqualTo(column1a).isNotEqualTo(column2);
		assertThat(column1.hashCode()).isEqualTo(column1a.hashCode()).isNotEqualTo(column2.hashCode());
		assertThat(column1.toString()).isEqualTo(column1a.toString()).isNotEqualTo(column2.toString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void columnCantBeInitializedWithNegativeNumber() throws Exception {
		new GridBoardColumn(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void columnCantBeInitializedWithNull() throws Exception {
		new GridBoardColumn(null);
	}
}
